ASFLAGS += -W
CFLAGS += -O1 -masm=intel -fno-asynchronous-unwind-tables

fibonacci: fibonacci.o


.PHONY: clean debug

clean:
	-rm fibonacci *.o

debug: CFLAGS+=-g
debug: fibonacci