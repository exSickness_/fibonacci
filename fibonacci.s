.intel_syntax noprefix

.data
.endptr:
	.quad 0x0
.fmt:
	.asciz "0x%lX%lX%lX%lX\n"
.INV_ARG:
	.asciz "Enter a number between 1-350 as first argument.\n"
.first:
	.quad 0x1 # offset 00
	.quad 0x0 # offset 08
	.quad 0x0 # offset 16
	.quad 0x0 # offset 24

.second:
	.quad 0x0 # offset 00
	.quad 0x0 # offset 08
	.quad 0x0 # offset 16
	.quad 0x0 # offset 24

.answer:
	.quad 0x0 # offset 00
	.quad 0x0 # offset 08
	.quad 0x0 # offset 16
	.quad 0x0 # offset 24


.text
.globl main
main:
	# check for one argument
	cmp 	rdi, 1
	je 		2f

	# extract iteration number
	mov 	rdi, [rsi+ 8]
	mov 	rsi, OFFSET .endptr
	mov 	edx, 10
	call	strtol

	# move iteration number into r14
	xor 	r14, r14
	mov 	r14, rax

	# validate iteration number
	cmp 	r14, 350
	jg		2f
	cmp 	r14, 0
	jle		2f

1:
	# clear out temporary register
	xor 	rax, rax

	# answer = first + second
	mov 	rax, [.first+0]
	add 	rax, [.second+0]
	mov 	[.answer+0], rax

	mov 	rax, [.first+8]
	adc 	rax, [.second+8]
	mov 	[.answer+8], rax

	mov 	rax, [.first+16]
	adc 	rax, [.second+16]
	mov 	[.answer+16], rax

	mov 	rax, [.first+24]
	adc 	rax, [.second+24]
	mov 	[.answer+24], rax

	# first = second
	mov 	rax, [.second+0]
	mov 	[.first+0], rax
	mov 	rax, [.second+8]
	mov 	[.first+8], rax
	mov 	rax, [.second+16]
	mov 	[.first+16], rax
	mov 	rax, [.second+24]
	mov 	[.first+24], rax

	# second = answer
	mov 	rax, [.answer+0]
	mov 	[.second+0], rax
	mov 	rax, [.answer+8]
	mov 	[.second+8], rax
	mov 	rax, [.answer+16]
	mov 	[.second+16], rax
	mov 	rax, [.answer+24]
	mov 	[.second+24], rax

	# subtract 1 from iteration number
	sub 	r14, 1
	jnz 	1b

	# print answer
	mov 	rdi, OFFSET .fmt
	mov 	rsi, [.answer+24]
	mov 	rdx, [.answer+16]
	mov 	rcx, [.answer+8]
	mov 	r8, [.answer+0]
	xor 	eax, eax
	call 	printf

	ret

2:
	mov 	rdi, OFFSET .INV_ARG
	xor 	eax, eax
	call 	printf
	ret
